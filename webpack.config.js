
const development = require('./config/webpack.development.js');
const production = require('./config/webpack.production.js');

const ENVIRONMENT = process.env.NODE_ENV || 'development';

const webpackConfig = { development, production };

const rootDir = __dirname;

module.exports = webpackConfig[ENVIRONMENT]({ rootDir });
