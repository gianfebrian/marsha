/* eslint-disable import/no-extraneous-dependencies */

const path = require('path');

const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const configurator = (env) => {
  const config = {
    entry: {
      vendor: path.join(env.rootDir, 'src', 'vendor.js'),
      boot: path.join(env.rootDir, 'src', 'boot.js'),
    },
    output: {
      path: path.join(env.rootDir, 'public'),
      filename: '[name].js',
    },

    module: {
      rules: [
        {
          test: /\.css$/,
          use: [
            {
              loader: 'style-loader',
            },
            {
              loader: 'css-loader',
              options: {
                alias: {
                  '../img': 'admin-lte/dist/img/',
                },
              },
            },
          ],
        },
        {
          test: /\.less$/,
          use: ['style-loader', 'css-loader', 'less-loader'],
        },
        {
          test: /\.json$/,
          use: ['json-loader'],
        },
        {
          test: /\.html$/,
          exclude: /(node_modules)/,
          use: ['raw-loader'],
        },
        {
          test: /\.(ttf|eot|svg|otf)$/,
          use: ['file-loader'],
        },
        {
          test: /\.woff(2)?$/,
          use: ['url-loader?limit=8192&minetype=application/font-woff'],
        },
        {
          test: /\.(png|jpg)$/,
          use: ['url-loader?limit=8192'],
        },
      ],

      noParse: [
        /.+zone\.js\/dist\/.+/, /.+angular2\/bundles\/.+/,
      ],
    },

    plugins: [
      new webpack.DefinePlugin({
        ENVIRONMENT: JSON.stringify('production'),
      }),
      new webpack.optimize.CommonsChunkPlugin({
        name: 'vendor',
        filename: 'vendor.js',
      }),
      new UglifyJsPlugin(),
    ],

    devtool: false,
  };

  return config;
};

module.exports = configurator;
