FROM node:6.11 as builder

RUN mkdir /app

ADD package.json /app/package.json

WORKDIR /app

RUN npm install

COPY . .

RUN npm run prod

FROM node:6.11-alpine

RUN mkdir /app

WORKDIR app

COPY --from=builder /app/docker/package.json package.json

RUN npm install

COPY --from=builder /app/index.js index.js

COPY --from=builder /app/public public

ENTRYPOINT [ "node", "index.js" ]
