
const { Component } = require('@angular/core');
const { Router } = require('@angular/router');

const { UserService } = require('./../../../auth/services/user/user.service');

const template = require('./header-navbar-user.template.html');

class HeaderNavbarUserComponent {
  constructor(userService, router) {
    this.userService = userService;
    this.router = router;
  }

  logout() {
    this.userService.logout();
    this.router.navigate(['/login']);
  }
}

HeaderNavbarUserComponent.annotations = [
  new Component({
    template,
    selector: '[header-navbar-user]',
  }),
];

HeaderNavbarUserComponent.parameters = [
  [UserService],
  [Router],
];

module.exports = { HeaderNavbarUserComponent };
