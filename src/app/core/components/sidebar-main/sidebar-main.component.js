
const { Component } = require('@angular/core');

const template = require('./sidebar-main.template.html');

class SidebarMainComponent {}

SidebarMainComponent.annotations = [
  new Component({
    template,
    selector: 'sidebar-main',
  }),
];

module.exports = { SidebarMainComponent };
