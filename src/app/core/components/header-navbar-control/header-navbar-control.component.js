
const { Component } = require('@angular/core');

const template = require('./header-navbar-control.template.html');

class HeaderNavbarControlComponent {}

HeaderNavbarControlComponent.annotations = [
  new Component({
    template,
    selector: 'header-navbar-control',
  }),
];

module.exports = { HeaderNavbarControlComponent };
