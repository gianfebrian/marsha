
const { Component } = require('@angular/core');

const template = require('./sidebar-search.template.html');

class SidebarSearchComponent {}

SidebarSearchComponent.annotations = [
  new Component({
    template,
    selector: '[sidebar-search]',
  }),
];

module.exports = { SidebarSearchComponent };
