
const { Component } = require('@angular/core');

const template = require('./sidebar-menu.template.html');

class SidebarMenuComponent {}

SidebarMenuComponent.annotations = [
  new Component({
    template,
    selector: '[sidebar-menu]',
  }),
];

module.exports = { SidebarMenuComponent };
