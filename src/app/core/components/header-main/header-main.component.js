
const { Component } = require('@angular/core');

const template = require('./header-main.template.html');

class HeaderMainComponent {}

HeaderMainComponent.annotations = [
  new Component({
    template,
    selector: 'header-main',
  }),
];

module.exports = { HeaderMainComponent };
