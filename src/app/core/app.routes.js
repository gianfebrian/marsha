
const { LoggedInGuard } = require('./guards/logged-in.guard');
const { LoggedOutGuard } = require('./guards/logged-out.guard');

const { HeaderMainComponent } = require('./components/header-main/header-main.component');
const { SidebarMainComponent } = require('./components/sidebar-main/sidebar-main.component');

const { LoginComponent } = require('./../auth/components/login/login.component');
const { PageHomeComponent } = require('./../pages/components/page-home/page-home.component');

const { TelegramCallbackDataComponent } = require('./../telegram/components/telegram-callback-data/telegram-callback-data.component');
const { TelegramMessageDataComponent } = require('./../telegram/components/telegram-message-data/telegram-message-data.component');
const { TelegramControlComponent } = require('./../telegram/components/telegram-control/telegram-control.component');

const routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [LoggedOutGuard],
  },
  {
    path: 'home',
    children: [
      {
        path: '',
        component: PageHomeComponent,
      },
      {
        path: '',
        component: HeaderMainComponent,
        outlet: 'header',
      },
      {
        path: '',
        component: SidebarMainComponent,
        outlet: 'sidebar',
      },
    ],
    canActivate: [LoggedInGuard],
  },
  {
    path: 'telegram-callback-data',
    children: [
      {
        path: '',
        component: TelegramCallbackDataComponent,
      },
      {
        path: '',
        component: HeaderMainComponent,
        outlet: 'header',
      },
      {
        path: '',
        component: SidebarMainComponent,
        outlet: 'sidebar',
      },
    ],
  },
  {
    path: 'telegram-message-data',
    children: [
      {
        path: '',
        component: TelegramMessageDataComponent,
      },
      {
        path: '',
        component: HeaderMainComponent,
        outlet: 'header',
      },
      {
        path: '',
        component: SidebarMainComponent,
        outlet: 'sidebar',
      },
    ],
  },
  {
    path: 'telegram-control',
    children: [
      {
        path: '',
        component: TelegramControlComponent,
      },
      {
        path: '',
        component: HeaderMainComponent,
        outlet: 'header',
      },
      {
        path: '',
        component: SidebarMainComponent,
        outlet: 'sidebar',
      },
    ],
  },
];

module.exports = { routes };
