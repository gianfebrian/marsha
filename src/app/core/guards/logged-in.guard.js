
const { Injectable } = require('@angular/core');
const { UserService } = require('./../../auth');

class LoggedInGuard {
  constructor(userService) {
    this.userService = userService;
  }

  canActivate() {
    return this.userService.verify().toPromise().then(res => res);
  }
}

LoggedInGuard.annotations = [
  new Injectable(),
];

LoggedInGuard.parameters = [
  [UserService],
];

module.exports = { LoggedInGuard };
