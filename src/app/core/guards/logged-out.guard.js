
const { Injectable } = require('@angular/core');
const { UserService } = require('./../../auth');

class LoggedOutGuard {
  constructor(userService) {
    this.userService = userService;
  }

  canActivate() {
    return this.userService.verify().toPromise().then(res => !res);
  }
}

LoggedOutGuard.annotations = [
  new Injectable(),
];

LoggedOutGuard.parameters = [
  [UserService],
];

module.exports = { LoggedOutGuard };
