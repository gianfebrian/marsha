
const _ = require('lodash');
const { Injectable } = require('@angular/core');
const { Http } = require('@angular/http');

const { StorageService } = require('./../storage/storage.service');
const { RequestService } = require('./../request/request.service');

class UserService {
  constructor(http, storageService, requestService) {
    this.http = http;
    this.storageService = storageService;
    this.requestService = requestService;
  }

  login(credentials) {
    const payload = JSON.stringify(credentials);
    const headers = this.requestService.getJsonHeaders();

    return this.http
      .post('/v1/auth/basic/validate', payload, { headers })
      .map(res => res.json())
      .map((res) => {
        if (res.success) {
          this.storageService.setAuthToken(res.token);
        }

        return res.success;
      });
  }

  logout() {
    this.storageService.removeAuthToken();
  }

  verify() {
    const token = this.storageService.getAuthToken();
    const headers = this.requestService.getJsonHeaders();

    return this.http
      .get(`/v1/auth/token/verify/${token}`, { headers })
      .map(res => res.json())
      .map(res => res.success || false)
  }
}

UserService.annotations = [
  new Injectable(),
];

UserService.parameters = [
  [Http],
  [StorageService],
  [RequestService],
];

module.exports = { UserService };
