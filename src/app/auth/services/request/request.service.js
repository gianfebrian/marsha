
const { Injectable } = require('@angular/core');
const { Headers } = require('@angular/http');

const { StorageService } = require('./../storage/storage.service.js');

class RequestService {
  constructor(storageService) {
    this.storageService = storageService;
  }

  getAuthHeaders() {
    const headers = this.getJsonHeaders();
    const authToken = this.storageService.getAuthToken();

    headers.append('Authorization', authToken);

    return headers;
  }

  // eslint-disable-next-line class-methods-use-this
  getJsonHeaders() {
    const headers = new Headers();

    headers.append('Content-Type', 'application/json');

    return headers;
  }
}

RequestService.annotations = [
  new Injectable(),
];

RequestService.parameters = [
  [StorageService],
];

exports.RequestService = RequestService;
