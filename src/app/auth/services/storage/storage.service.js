/* global localStorage */
/* eslint-disable
  class-methods-use-this
*/

const STORAGE_KEY = 'auth_token';

class StorageService {
  getAuthToken() {
    return localStorage.getItem(STORAGE_KEY);
  }

  setAuthToken(token) {
    localStorage.setItem(STORAGE_KEY, token);
  }

  removeAuthToken() {
    localStorage.removeItem(STORAGE_KEY);
  }
}

exports.StorageService = StorageService;
