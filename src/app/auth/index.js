const { StorageService } = require('./services/storage/storage.service');
const { RequestService } = require('./services/request/request.service');
const { UserService } = require('./services/user/user.service');

const { LoginComponent } = require('./components/login/login.component');

module.exports = {
  StorageService,
  RequestService,
  UserService,

  AUTH_PROVIDERS: [
    StorageService,
    RequestService,
    UserService,
  ],

  AUTH_DECLARATIONS: [
    LoginComponent,
  ],
};
