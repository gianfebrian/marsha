
const { Injectable } = require('@angular/core');
const { Http } = require('@angular/http');

const { RequestService } = require('./../../auth/services/request/request.service');

class TelegramService {
  constructor(http, requestService) {
    this.http = http;
    this.requestService = requestService;
    this.headers = this.requestService.getAuthHeaders();
  }

  getTelegramCallbackData() {
    return this.http
      .get('/v1/telegram/callback', { headers: this.headers })
      .map(res => res.json())
      .map(res => res);
  }

  getTelegramMessageData() {
    return this.http
      .get('/v1/telegram/message', { headers: this.headers })
      .map(res => res.json())
      .map(res => res);
  }

  startTelegram(scenarioURI, token) {
    const payload = { scenario_uri: scenarioURI, token };
    return this.http
      .post('/v1/telegram/start', payload, { headers: this.headers });
  }

  stopTelegram() {
    return this.http
      .post('/v1/telegram/stop', null, { headers: this.headers });
  }
}

TelegramService.annotations = [
  new Injectable(),
];

TelegramService.parameters = [
  [Http],
  [RequestService],
];

module.exports = { TelegramService };
