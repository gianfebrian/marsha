
const _ = require('lodash');

const { Router } = require('@angular/router');
const { Component } = require('@angular/core');
const { BehaviorSubject } = require('rxjs/BehaviorSubject');

const { TelegramService } = require('./../../services/telegram.service');

const template = require('./telegram-callback-data.template.html');

class TelegramCallbackDataComponent {
  constructor(router, telegramService) {
    this.infoMessage = null;
    this.rows = new BehaviorSubject([]);

    this.router = router;

    this.telegramService = telegramService;
  }

  ngOnInit() {
    this.getTableData();
  }

  getTableData() {
    this.telegramService.getTelegramCallbackData().subscribe((result) => {
      if (_.isEmpty(result)) {
        return this.rows;
      }

      return this.rows.next(result);
    });
  }
}

TelegramCallbackDataComponent.annotations = [
  new Component({
    template,
    selector: 'telegram-callback-data',
  }),
];

TelegramCallbackDataComponent.parameters = [
  [Router],
  [TelegramService],
];

module.exports = { TelegramCallbackDataComponent };
