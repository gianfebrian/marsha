
const _ = require('lodash');

const { Router } = require('@angular/router');
const { Component } = require('@angular/core');
const { BehaviorSubject } = require('rxjs/BehaviorSubject');

const { TelegramService } = require('./../../services/telegram.service');

const template = require('./telegram-message-data.template.html');

class TelegramMessageDataComponent {
  constructor(router, telegramService) {
    this.infoMessage = null;
    this.rows = new BehaviorSubject([]);

    this.router = router;

    this.telegramService = telegramService;
  }

  ngOnInit() {
    this.getTableData();
  }

  getTableData() {
    this.telegramService.getTelegramMessageData().subscribe((result) => {
      if (_.isEmpty(result)) {
        return this.rows;
      }

      return this.rows.next(result);
    });
  }
}

TelegramMessageDataComponent.annotations = [
  new Component({
    template,
    selector: 'telegram-message-data',
  }),
];

TelegramMessageDataComponent.parameters = [
  [Router],
  [TelegramService],
];

module.exports = { TelegramMessageDataComponent };
