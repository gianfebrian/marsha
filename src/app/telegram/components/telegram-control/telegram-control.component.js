
const { Component } = require('@angular/core');

const { TelegramService } = require('./../../services/telegram.service');

const template = require('./telegram-control.template.html');

class TelegramControlComponent {
  constructor(telegramService) {
    this.infoMessage = null;
    this.scenarioURI = '';
    this.token = '';

    this.telegramService = telegramService;
  }

  onStart() {
    this.telegramService.startTelegram(this.scenarioURI, this.token).subscribe();
  }

  onStop() {
    this.telegramService.stopTelegram().subscribe();
  }
}

TelegramControlComponent.annotations = [
  new Component({
    template,
    selector: 'telegram-control',
  }),
];

TelegramControlComponent.parameters = [
  [TelegramService],
];

module.exports = { TelegramControlComponent };
