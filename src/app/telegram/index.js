
const { TelegramCallbackDataComponent } = require('./components/telegram-callback-data/telegram-callback-data.component');
const { TelegramMessageDataComponent } = require('./components/telegram-message-data/telegram-message-data.component');
const { TelegramControlComponent } = require('./components/telegram-control/telegram-control.component');

const { TelegramService } = require('./services/telegram.service');

module.exports = {
  TELEGRAM_DECLARATIONS: [
    TelegramCallbackDataComponent,
    TelegramMessageDataComponent,
    TelegramControlComponent,
  ],

  TELEGRAM_PROVIDERS: [
    TelegramService,
  ],
};
