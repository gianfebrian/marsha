
const { PageHomeComponent } = require('./components/page-home/page-home.component');

module.exports = {
  PageHomeComponent,

  PAGE_DECLARATIONS: [
    PageHomeComponent,
  ],
};
