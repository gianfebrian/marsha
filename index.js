
const path = require('path');
const express = require('express');
const proxy = require('http-proxy-middleware');

const app = express();

app.use('/v1', proxy({
  target: process.env.API_URI,
  changeOrigin: true,
}));

app.use(express.static(path.join(__dirname, 'public')));
app.listen(process.env.APP_PORT, process.env.APP_HOST);

process.on('SIGINT', () => process.exit(0));
